﻿namespace CVRMoreParams
{
    public enum EParamType
    {
        Bool,
        Int,
        Float
    }
}