using UnityEngine;

namespace CVRMoreParams
{
    internal static class Extensions
    {
        // https://github.com/SDraw/ml_mods_cvr/blob/8c014e40d3cbcff2c21074c124fcc5a1f887536b/ml_amt/Utils.cs#L42
        public static Matrix4x4 GetMatrix(this Transform p_transform, bool p_pos = true, bool p_rot = true, bool p_scl = false)
        {
            return Matrix4x4.TRS(p_pos ? p_transform.position : Vector3.zero, p_rot ? p_transform.rotation : Quaternion.identity, p_scl ? p_transform.localScale : Vector3.one);
        }
    }
}