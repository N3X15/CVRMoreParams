﻿using ABI.CCK.Components;
using ABI_RC.Core;
using ABI_RC.Core.InteractionSystem;
using ABI_RC.Core.Player;

namespace CVRMoreParams
{
    public abstract class BaseParamProvider
    {
        public BaseParamProvider(bool isEnabled)
        {
            IsEnabled = isEnabled;
        }

        public bool IsEnabled { get; set; }
        public EParamType ParamType { get; }

        ///<summary>
        /// Display name of this provider.
        ///</summary>
        public abstract string Name { get; }
        public abstract void OnLateUpdate(CVRPlayerEntity player, PuppetMaster puppetMaster, CVRAnimatorManager animManager, CVRAvatar avatar, CVRVisemeController visemeController);



        protected void SendSyncedParameterInt(CVRPlayerEntity player, CVRAnimatorManager animManager, string paramName, int value)
        {
            animManager.SetAnimatorParameterInt(paramName, value);
            if (player == null && CVRMoreParamsMod.hasParamStateChanged(paramName, value))
            {
                CVR_MenuManager.Instance.SendAdvancedAvatarUpdate(paramName, value, false);
                CVRMoreParamsMod.storeParamState(this, paramName, value);
            }
        }
        protected void SendSyncedParameterFloat(CVRPlayerEntity player, CVRAnimatorManager animManager, string paramName, float value)
        {
            animManager.SetAnimatorParameterFloat(paramName, value);
            if (player == null && CVRMoreParamsMod.hasParamStateChanged(paramName, value))
            {
                CVR_MenuManager.Instance.SendAdvancedAvatarUpdate(paramName, value, false);
                CVRMoreParamsMod.storeParamState(this, paramName, value);
            }
        }
        protected void SendSyncedParameterBool(CVRPlayerEntity player, CVRAnimatorManager animManager, string paramName, bool value)
        {
            animManager.SetAnimatorParameterBool(paramName, value);
            if (player == null && CVRMoreParamsMod.hasParamStateChanged(paramName, value))
            {
                CVR_MenuManager.Instance.SendAdvancedAvatarUpdate(paramName, value ? 1f : 0f, false);
                CVRMoreParamsMod.storeParamState(this, paramName, value);
            }
        }
    }
}