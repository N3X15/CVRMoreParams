﻿using ABI.CCK.Components;
using ABI_RC.Core;
using ABI_RC.Core.Player;
using HarmonyLib;
using MelonLoader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CVRMoreParams
{
    public class CVRMoreParamsMod : MelonMod
    {
        public List<BaseParamProvider> Providers = new();
        private FieldInfo fiAnimatorManager;
        private FieldInfo fiAvatar;
        public static CVRMoreParamsMod Instance;
        private static List<Action> whenReadySubscribers = new();
        internal static Dictionary<string, object> localParamState = new();
        internal static Dictionary<string, BaseParamProvider> providerOfParamState = new();

        public override void OnInitializeMelon()
        {
            Instance = this;

            if (!Utils.TryGetDeclaredField(typeof(PuppetMaster), "_animatorManager", out fiAnimatorManager))
                return;
            if (!Utils.TryGetDeclaredField(typeof(PuppetMaster), "_avatar", out fiAvatar))
                return;

            WhenReady(() =>
            {
                RegisterProvider(new AngularVelocityProvider());
                RegisterProvider(new EVisemeProvider());
                RegisterProvider(new IsFocusedProvider());
                RegisterProvider(new IsLocalProvider());
                RegisterProvider(new LinearVelocityProvider());
                RegisterProvider(new UprightProvider());
            });

            for (int i = 0; i < whenReadySubscribers.Count; i++)
                whenReadySubscribers[i].Invoke();
        }

        public static void RegisterProvider(BaseParamProvider paramProvider)
        {
            if (Instance is null)
            {
                throw new PluginNotReadyException();
            }
            Instance.Providers.Add(paramProvider);
        }

        public static void WhenReady(Action cb)
        {
            whenReadySubscribers.Add(cb);
        }

        internal static bool hasParamStateChanged(string paramID, object val)
        {
            return (!localParamState.ContainsKey(paramID)) || (localParamState[paramID] != val);
        }

        internal static void storeParamState(BaseParamProvider provider, string paramID, object val)
        {
            localParamState[paramID] = val;
            providerOfParamState[paramID] = provider;
        }

        public override void OnLateUpdate()
        {
            // Wait for a valid state
            if (CVRPlayerManager.Instance == null)
                return;
            if (fiAnimatorManager == null || fiAvatar == null)
                return;

            // Do stuff on our own screen.
            HandleLocalPlayer();

            // Do stuff to other players.
            CVRPlayerManager.Instance.NetworkPlayers
                .Where((CVRPlayerEntity cvrpe) => !string.IsNullOrEmpty(cvrpe.Uuid))
                .Do(HandleNetworkPlayer);
        }

        private void HandleLocalPlayer()
        {
            if (PlayerSetup.Instance == null || PlayerSetup.Instance._avatar == null || PlayerSetup.Instance.animatorManager == null)
                return;
            // Local player does not have a puppetmaster.
            var avatarGo = PlayerSetup.Instance._avatar;
            var visemeController = avatarGo.GetComponent<CVRVisemeController>();
            var avatar = avatarGo.GetComponent<CVRAvatar>();
            var animManager = PlayerSetup.Instance.animatorManager;
            BaseParamProvider module;
            lock (Providers)
            {
                for (var i = 0; i < Providers.Count; i++)
                {
                    module = Providers[i];
                    if (module.IsEnabled)
                        module.OnLateUpdate(null, null, animManager, avatar, visemeController);
                }
            }
        }


        private void HandleNetworkPlayer(CVRPlayerEntity player)
        {
            if (player == null)
                return;

            //LoggerInstance.Msg($"{player.Username}\t{player.Uuid}\t{player.PlayerDescriptor.ownerId}");
            var puppetMaster = player.PuppetMaster;
            if (puppetMaster == null || puppetMaster.avatarObject == null)
                return;
            var animManager = fiAnimatorManager.GetValue(puppetMaster) as CVRAnimatorManager;
            var avatar = fiAvatar.GetValue(puppetMaster) as CVRAvatar;
            var visemeController = puppetMaster.avatarObject.GetComponent<CVRVisemeController>();

            //if (puppetMaster == null || animManager == null || avatar == null || visemeController == null)
            //    return;

            // Our params are implemented as modules for quick and dirty implementation and segmentation.
            //LoggerInstance.Msg($"{player.Username}\t{player.Uuid}\t{animManager!=null}\t{avatar!=null}")

            BaseParamProvider module;
            lock (Providers)
            {
                for (var i = 0; i < Providers.Count; i++)
                {
                    module = Providers[i];
                    if (module.IsEnabled)
                        module.OnLateUpdate(player, puppetMaster, animManager, avatar, visemeController);
                }
            }
        }
    }
}
