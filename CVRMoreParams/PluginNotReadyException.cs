using System;
using System.Runtime.Serialization;

namespace CVRMoreParams
{
    [Serializable]
    internal class PluginNotReadyException : Exception
    {
        public PluginNotReadyException()
        {
        }

        public PluginNotReadyException(string message) : base(message)
        {
        }

        public PluginNotReadyException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected PluginNotReadyException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}