using ABI.CCK.Components;
using ABI_RC.Core;
using ABI_RC.Core.Player;
using UnityEngine;

namespace CVRMoreParams
{
    public class AngularVelocityProvider : BaseParamProvider
    {
        public static readonly string PARAM_ANGULAR_X = "CVRMP_AngularX";
        public static readonly string PARAM_ANGULAR_Y = "CVRMP_AngularY";
        public static readonly string PARAM_ANGULAR_Z = "CVRMP_AngularZ";

        public override string Name => "AngularXYZ";
        private Vector3 lastEulers = Vector3.zero;
        private Vector3 angularVelocity = Vector3.zero;

        public float AngularX { get; private set; }
        public float AngularY { get; private set; }
        public float AngularZ { get; private set; }

        public AngularVelocityProvider() : base(isEnabled: true)
        {
        }
        public override void OnLateUpdate(CVRPlayerEntity player, PuppetMaster puppetMaster, CVRAnimatorManager animManager, CVRAvatar avatar, CVRVisemeController visemeController)
        {
            if (PlayerSetup.Instance && PlayerSetup.Instance._avatar)
            {
                var eulers = PlayerSetup.Instance._avatar.transform.rotation.eulerAngles;
                angularVelocity = eulers - lastEulers;
                lastEulers = eulers;
                SendSyncedParameterFloat(player, animManager, PARAM_ANGULAR_X, AngularX = angularVelocity.x);
                SendSyncedParameterFloat(player, animManager, PARAM_ANGULAR_Y, AngularY = angularVelocity.y);
                SendSyncedParameterFloat(player, animManager, PARAM_ANGULAR_Z, AngularZ = angularVelocity.z);
            }
            else
            {
                SendSyncedParameterFloat(player, animManager, PARAM_ANGULAR_X, 0f);
                SendSyncedParameterFloat(player, animManager, PARAM_ANGULAR_Y, 0f);
                SendSyncedParameterFloat(player, animManager, PARAM_ANGULAR_Z, 0f);
            }
        }
    }
}