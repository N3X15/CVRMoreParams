﻿using ABI.CCK.Components;
using ABI_RC.Core;
using ABI_RC.Core.Player;
using System.Reflection;

namespace CVRMoreParams
{
    /// <summary>
    /// Attempt to tell the game which viseme is currently active.
    /// 
    /// This is an iffy implementation, since the original actually does some blending, but it'll work for the meantime.
    /// </summary>
    public class EVisemeProvider : BaseParamProvider
    {
        public enum EViseme : int
        {
            SIL = 0,
            PP,
            FF,
            TH,
            DD,
            KK,
            CH,
            SS,
            NN,
            RR,
            AA,
            E,
            IH,
            OH,
            OU,
        }
        // 
        public static readonly string PARAM_EVISEME_INDEX = "CVRMP_EVisemeIndex";
        public static readonly string PARAM_EVISEME_WEIGHT = "CVRMP_EVisemeWeight";

        //private readonly FieldInfo fiVisemeBlendShapes;
        //private readonly FieldInfo fiVisemeWeights;
        //private readonly FieldInfo fiAnalysisValue;
        
        private readonly FieldInfo fiMorphTarget;
        private readonly FieldInfo fiContext;

        public EVisemeProvider() : base(isEnabled: true)
        {
            PrimaryVisemeIndex = 0;
            PrimaryVisemeWeight = 0f;
            VisemeBlendShapes = new int[OVRLipSync.VisemeCount];
            VisemeWeights = new float[OVRLipSync.VisemeCount];
            // if (!Utils.TryGetDeclaredField(typeof(CVRVisemeController), "visemeBlendShapes", out fiVisemeBlendShapes))
            // {
            //     IsEnabled = false;
            // }
            // if (!Utils.TryGetDeclaredField(typeof(CVRVisemeController), "visemeWeights", out fiVisemeWeights))
            // {
            //     IsEnabled = false;
            // }
            // if (!Utils.TryGetDeclaredField(typeof(CVRVisemeController), "analysisValue", out fiAnalysisValue))
            // {
            //     IsEnabled = false;
            // }

            if (!Utils.TryGetDeclaredField(typeof(CVRVisemeController), "_morphTarget", out fiMorphTarget))
            {
                IsEnabled = false;
            }
            if (!Utils.TryGetDeclaredField(typeof(CVRVisemeController), "_context", out fiContext))
            {
                IsEnabled = false;
            }
        }
        public override string Name => "EViseme";


        public int[] VisemeBlendShapes { get; private set; }
        public float[] VisemeWeights { get; private set; }
        public int PrimaryVisemeIndex { get; private set; }
        public EViseme PrimaryViseme
        {
            get
            {
                return (EViseme)PrimaryVisemeIndex;
            }
        }
        public float PrimaryVisemeWeight { get; private set; }

        private (int, float) GetMostDominantBlendshape(float[] blendshapes)
        {
            float mv = float.NegativeInfinity;
            int mi = -1;
            for (int i = 0; i < OVRLipSync.VisemeCount; i++)
            {
                if (mv < blendshapes[i])
                {
                    mv = blendshapes[i];
                    mi = i;
                }
            }

            if (mi == -1)
            {
                mi = 0;
                mv = 0f;
            }
            return (mi, mv);
        }

        public override void OnLateUpdate(CVRPlayerEntity player, PuppetMaster puppetMaster, CVRAnimatorManager animManager, CVRAvatar avatar, CVRVisemeController visemeController)
        {
            if (visemeController == null)
            {
                SendSyncedParameterInt(player, animManager, PARAM_EVISEME_INDEX, 0);
                SendSyncedParameterFloat(player, animManager, PARAM_EVISEME_WEIGHT, 0f);
                return;
            }
            var ovrContext = (CVROculusLipSyncContext)fiContext.GetValue(visemeController);
            if (visemeController == null || ovrContext == null)
            {
                SendSyncedParameterInt(player, animManager, PARAM_EVISEME_INDEX, 0);
                SendSyncedParameterFloat(player, animManager, PARAM_EVISEME_WEIGHT, 0f);
                return;
            }

            var frame = ovrContext.GetCurrentPhonemeFrame();
            var (maxIndex, maxWeight) = GetMostDominantBlendshape(frame.Visemes);
            PrimaryVisemeIndex = maxIndex;
            PrimaryVisemeWeight = maxWeight;
            SendSyncedParameterInt(player, animManager, PARAM_EVISEME_INDEX, PrimaryVisemeIndex);
            SendSyncedParameterFloat(player, animManager, PARAM_EVISEME_WEIGHT, PrimaryVisemeWeight);
            VisemeWeights = frame.Visemes;
            var morphTarget = (OVRLipSyncContextMorphTarget)fiMorphTarget.GetValue(visemeController);
            if (morphTarget != null && morphTarget.visemeToBlendTargets != null &&
                morphTarget.visemeToBlendTargets.Length == OVRLipSync.VisemeCount)
            {
                VisemeBlendShapes = morphTarget.visemeToBlendTargets;
            }
        }
    }
}