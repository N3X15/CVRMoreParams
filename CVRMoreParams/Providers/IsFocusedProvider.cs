﻿using ABI.CCK.Components;
using ABI_RC.Core.Player;
using ABI_RC.Core;
using UnityEngine;

namespace CVRMoreParams
{
    public class IsFocusedProvider : BaseParamProvider
    {
        public static readonly string PARAM_IS_FOCUSED = "#CVRMP_IsFocused";

        public override string Name => "IsFocused";

        public IsFocusedProvider() : base(isEnabled: true)
        {
        }

        public override void OnLateUpdate(CVRPlayerEntity player, PuppetMaster puppetMaster, CVRAnimatorManager animManager, CVRAvatar avatar, CVRVisemeController visemeController)
        {
            SendSyncedParameterBool(player, animManager, PARAM_IS_FOCUSED, Application.isFocused);
        }
    }
}
