﻿using ABI.CCK.Components;
using ABI_RC.Core;
using ABI_RC.Core.Player;

namespace CVRMoreParams
{
    /// <summary>
    /// Tell animators whether we are rendering the avatar on our own machine or not.
    /// 
    /// Useful for HUDs with annoying shaders.
    /// </summary>
    public class IsLocalProvider : BaseParamProvider
    {
        public static readonly string PARAM_IS_LOCAL = "#CVRMP_IsLocal";

        public override string Name => "IsLocal";

        public IsLocalProvider() : base(isEnabled: true)
        {
        }

        public override void OnLateUpdate(CVRPlayerEntity player, PuppetMaster puppetMaster, CVRAnimatorManager animManager, CVRAvatar avatar, CVRVisemeController visemeController)
        {
            SendSyncedParameterBool(player, animManager, PARAM_IS_LOCAL, player == null);
        }
    }
}