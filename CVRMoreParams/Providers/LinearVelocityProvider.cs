using ABI.CCK.Components;
using ABI_RC.Core;
using ABI_RC.Core.Player;
using UnityEngine;

namespace CVRMoreParams
{
    public class LinearVelocityProvider : BaseParamProvider
    {
        public static readonly string PARAM_VELOCITY_X = "CVRMP_VelocityX";
        public static readonly string PARAM_VELOCITY_Y = "CVRMP_VelocityY";
        public static readonly string PARAM_VELOCITY_Z = "CVRMP_VelocityZ";

        public override string Name => "VelocityXYZ";
        private Vector3 lastEulers = Vector3.zero;
        private Vector3 linearVelocity = Vector3.zero;

        public float VelocityX { get; private set; }
        public float VelocityY { get; private set; }
        public float VelocityZ { get; private set; }

        public LinearVelocityProvider() : base(isEnabled: true)
        {
        }
        public override void OnLateUpdate(CVRPlayerEntity player, PuppetMaster puppetMaster, CVRAnimatorManager animManager, CVRAvatar avatar, CVRVisemeController visemeController)
        {
            if (PlayerSetup.Instance && PlayerSetup.Instance._avatar)
            {
                var pos = PlayerSetup.Instance._avatar.transform.position;
                linearVelocity = pos - lastEulers;
                lastEulers = pos;
                SendSyncedParameterFloat(player, animManager, PARAM_VELOCITY_X, VelocityX = linearVelocity.x);
                SendSyncedParameterFloat(player, animManager, PARAM_VELOCITY_Y, VelocityY = linearVelocity.y);
                SendSyncedParameterFloat(player, animManager, PARAM_VELOCITY_Z, VelocityZ = linearVelocity.z);
            }
            else
            {
                SendSyncedParameterFloat(player, animManager, PARAM_VELOCITY_X, 0f);
                SendSyncedParameterFloat(player, animManager, PARAM_VELOCITY_Y, 0f);
                SendSyncedParameterFloat(player, animManager, PARAM_VELOCITY_Z, 0f);
            }
        }
    }
}