using ABI.CCK.Components;
using ABI_RC.Core;
using ABI_RC.Core.InteractionSystem;
using ABI_RC.Core.Player;
using UnityEngine;

namespace CVRMoreParams
{
    public class UprightProvider : BaseParamProvider
    {
        public static readonly string PARAM_UPRIGHT = "CVRMP_Upright";
        static readonly Vector4 POINT_VECTOR = new(0f,0f,0f,1f);
        //private FieldInfo ms_rootVelocity;
        //private FieldInfo ms_groundedRaw;
        public float Upright {get; private set;}
        public override string Name => "Upright";

        public UprightProvider() : base(isEnabled: true)
        {
            //https://github.com/SDraw/ml_mods_cvr/blob/f364d6d2aa4ee45d6d23e41d272b0de059a028e6/ml_amt/MotionTweaker.cs#L11-12
            //if(!Utils.TryGetDeclaredField(typeof(IKSolverVR), "rootVelocity", out ms_rootVelocity)) 
            //{
            //    IsEnabled = false;
            //}
            //if (!Utils.TryGetDeclaredField(typeof(MovementSystem), "_isGroundedRaw", out ms_groundedRaw))
            //{
            //    IsEnabled = false;
            //}
        }
        public override void OnLateUpdate(CVRPlayerEntity player, PuppetMaster puppetMaster, CVRAnimatorManager animManager, CVRAvatar avatar, CVRVisemeController visemeController)
        {
            // Idiot checks
            if(PlayerSetup.Instance==null || CVR_MenuManager.Instance == null || CVR_MenuManager.Instance.coreData == null || CVR_MenuManager.Instance.coreData.core==null || PlayerSetup.Instance._avatar == null)
                return;
            //https://github.com/SDraw/ml_mods_cvr/blob/f364d6d2aa4ee45d6d23e41d272b0de059a028e6/ml_amt/MotionTweaker.cs#L77
            Matrix4x4 l_hmdMatrix = PlayerSetup.Instance.transform.GetMatrix().inverse * (CVR_MenuManager.Instance.coreData.core.inVr ? PlayerSetup.Instance.vrHeadTracker.transform.GetMatrix() : PlayerSetup.Instance.desktopCameraRig.transform.GetMatrix());
            float l_currentHeight = Mathf.Clamp((l_hmdMatrix * POINT_VECTOR).y, 0f, float.MaxValue);
            float l_avatarViewHeight = Mathf.Clamp(PlayerSetup.Instance.GetViewWorldPosition().y * PlayerSetup.Instance._avatar.transform.localScale.y, 0f, float.MaxValue);
            Upright = Mathf.Clamp((((l_currentHeight > 0f) && (l_avatarViewHeight > 0f)) ? (l_currentHeight / l_avatarViewHeight) : 0f), 0f, 1f);
            SendSyncedParameterFloat(player, animManager, PARAM_UPRIGHT, Upright);
            
            // TODO?
            //PoseState l_poseState = (Upright <= m_proneLimit) ? PoseState.Proning : ((Upright <= m_crouchLimit) ? PoseState.Crouching : PoseState.Standing);
        }
    }
}