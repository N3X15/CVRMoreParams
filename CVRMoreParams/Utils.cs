﻿using ABI_RC.Core.Player;
using System;
using System.Reflection;

namespace CVRMoreParams
{
    internal static class Utils
    {
        internal static Type ParamTypeToType(EParamType v)
        {
            switch (v)
            {
                case EParamType.Bool: return typeof(bool);
                case EParamType.Int: return typeof(int);
                case EParamType.Float: return typeof(float);
            }
            return typeof(bool);
        }

        internal static bool PlayerIsLocal(CVRPlayerEntity player)
        {
            return player == null || player.Uuid.Equals("_PLAYERLOCAL");
        }

        internal static bool TryGetDeclaredField(Type T, string fieldName, out FieldInfo fi)
        {
            fi = null;
            CVRMoreParamsMod.Instance.LoggerInstance.Msg($"Attempting to find field {T.Name}.{fieldName}:");
            var _fi = HarmonyLib.AccessTools.DeclaredField(T, fieldName);
            if (_fi == null)
            {
                CVRMoreParamsMod.Instance.LoggerInstance.Error("  Failed!");
                return false;
            }
            else
            {
                CVRMoreParamsMod.Instance.LoggerInstance.Msg("  OK!");
                fi = _fi;
                return true;
            }
        }
    }
}
