# CVRMoreParams

This is a modular little mod that gives you a few crucial animator parameters for avatars.  These parameters have been floated to devs, but <s>they're</s> we're busy and I'm impatient.

**NOTE:** Some of these are in the game now.

<table>
<thead>
<caption>Current Parameters</caption>
<tr>
<th>ParamID</th>
<th>Provider</th>
<th>Type</th>
<th>Range</th>
<th>Default</th>
<th>Notes</th>
</tr>
</thead>
<tbody>
<tr>
<th><code>#CVRMP_IsLocal</code></th>
<td><a href="CVRMoreParams/Providers/IsLocalProvider.cs">IsLocalProvider</a></td>
<td>boolean</td>
<td>&nbsp;</td>
<td>false</td>
<td>Where the avatar is being rendered locally or not.  Useful for HUDs.</td>
</tr>
<tr>
<th><code>CVRMP_EVisemeIndex</code></th>
<td><a href="CVRMoreParams/Providers/EVisemeProvider.cs">EVisemeProvider</a></td>
<td>integer</td>
<td>[0,14]</td>
<td>0</td>
<td>Which viseme is the most dominant.</td>
</tr>
<tr>
<th><code>CVRMP_EVisemeWeight</code></th>
<td><a href="CVRMoreParams/Providers/EVisemeProvider.cs">EVisemeProvider</a></td>
<td>float</td>
<td>[0,100)</td>
<td>0.0</td>
<td>Weight of the most dominant viseme blendshape.</td>
</tr>
<tr>
<th><code>CVRMP_Upright</code></th>
<td><a href="CVRMoreParams/Providers/UprightProvider.cs">UprightProvider</a></td>
<td>float</td>
<td>[0,1)</td>
<td>0.0</td>
<td>How upright your character is (1=upright, 0=prone)</td>
</tr>
<tr>
<th><code>CVRMP_Angular(X|Y|Z)</code></th>
<td><a href="CVRMoreParams/Providers/AngularVelocityProvider.cs">AngularVelocityProvider</a></td>
<td>float</td>
<td>[-∞,∞]</td>
<td>0.0</td>
<td>Angular velocity along the given axis, in radians per second.</td>
</tr>
</tbody>
</table>

## Planned

* Supine?
* Clean up all the crap NaK already added to the game

## Legal Buttocks Coverage

**I now work for Alpha Blend Interactive.  This project is not an official project and is not administered, sanctioned, nor approved by Alpha Blend Interactive.**  This is a personal project that I work on in my spare time, of which I have very little.  I put this here in case someone finds it useful.

This software is available to you under the [MIT Open-Source License](./LICENSE), as well as the following Terms.

**By using unofficial third-party modifications such as this software with ChilloutVR, you risk degrading your gameplay experience via crashes and unintended behaviour.** While we attempt to reduce these problems as much as we can, unintended side effects can occur, especially after updates.

**This modification is unofficial and not supported by Alpha Blend Interactive. Using this modification might cause issues with performance, security or stability of the games.**

**By downloading this software, you accept these risks and agree that the authors, maintainers, and copyright holders are exempt from any such claim, damages, or liability.**

This project is not contributed to, affiliated with, nor condoned by Alpha Blend Interactive, the ChilloutVR Team, or their representatives in any official capacity. The trademarks "ChilloutVR", "CVR", "Alpha Blend Interactive", and "ABI" are used in this software in a referential capacity, which is permitted under US Trademark Law (15 U.S.C. &sect; 1115(b)(4)) and case-law precedent.

We also attempt to comply with [ChilloutVR Terms of Service &para; 7 &sect; 1-7](https://documentation.abinteractive.net/official/legal/tos/#7-modding-our-games) (Sept 15 2022 - "Modding our games"), which regulates mods for CVR, as well as [CVR ToS &para; 9](https://documentation.abinteractive.net/official/legal/tos/#9-third-party-applications-or-sites) (Sept 15 2022 - "Third party applications or sites"), which mandates legal boilerplate information.

No electric pixies were harmed in the making of this software.

(Feel free to use this text, with or without modifications, under the MIT License (tl;dr credit me). <abbr title="I Am Not A Lawyer">IANAL</abbr>, but I've had lots of experience with this shit.)

## Shout-outs

* ChilloutVR
* CVRMG Discord
* SDraw (we use some of [their code](https://github.com/SDraw/ml_mods_cvr) for calculating Upright.)

## Compiling

### Prerequisites

* .NET 8
* Python >= 3.11 (for MelonTool)

### via MelonTool (Recommended)

MelonTool generates the CSProj files and feeds the correct parameters to `dotnet` to build the project.

To install (assuming you installed Python to your PATH):

```shell
# Install or update melontool from git
pip install -U git+https://gitlab.com/Scottinator/melontool.git
```

Now to build this project:

```shell
# Change directory to this project
cd path/to/CVRMoreParams

# Regenerate the .csproj file (if it can't automatically find CVR, feed it in as -P "C:\path\to\chillout")
melontool generate

# Build the project with .NET
melontool build

# Deploy to gaem
melontool deploy
```

### via dotnet

If you just want to compile the project, **you will first need to fix the csproj hint paths manually!**  Search and replace `G:\SteamLibrary\steamapps\common\ChilloutVR` with wherever you installed CVR.

Then:
```shell
# Change directory to this project
cd path/to/CVRMoreParams

# Compile
dotnet build -c Release CVRMoreParams.csproj
```

You will find your binary in `dist/`.

If you rebuild this software often, you may consider making a file called `DEPLOY.bat` with the contents:

```cmd
copy /y dist\net48\CVRMoreParams.dll D:\path\to\ChilloutVR\Mods\
```

For Bash users, this equates to:

```bash
cp dist/net48/CVRMoreParams.dll /path/to/ChilloutVR/Mods/
```
